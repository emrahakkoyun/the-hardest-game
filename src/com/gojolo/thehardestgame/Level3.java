package com.gojolo.thehardestgame;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class Level3 extends Activity implements View.OnTouchListener{
	ImageView devil;
	ImageView al1,al2,al3,al4,al5,al6,al7,al8,kapi;
static	ImageView bck1,bck2,bck3;
	boolean a1=false,a2=false,a3=false,a4=false,a5=false,a6=false,a7=false,a8=false;
	boolean ses1=true,ses2=true,ses3=true,ses4=true,ses5=true,ses6=true,ses7=true,ses8=true;
	boolean move;
	float nx,ny;
	int screenHeight,screenWidth;
	int sayac=0;
	private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_level3);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		AdRequest adRequest = new AdRequest.Builder().build(); 
	    interstitial = new InterstitialAd(Level3.this);
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/6410661047");
		interstitial.loadAd(adRequest);
				interstitial.setAdListener(new AdListener() {
					public void onAdLoaded() {
						displayInterstitial();
					}
				});
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		RelativeLayout rlt = (RelativeLayout)findViewById(R.id.rlt);
		bck1 = new ImageView(this);
		bck2 = new ImageView(this);
		bck3 = new ImageView(this);
		al1 = new ImageView(this);
		al2 = new ImageView(this);
		al3 = new ImageView(this);
		al4 = new ImageView(this);
		al5 = new ImageView(this);
		al6 = new ImageView(this);
		al7 = new ImageView(this);
		al8 = new ImageView(this);
		kapi = new ImageView(this);
		bck1.setBackgroundResource(R.drawable.abck);
		bck2.setBackgroundResource(R.drawable.ybck);
		bck3.setBackgroundResource(R.drawable.rbck);
		al1.setBackgroundResource(R.drawable.gold);
		al2.setBackgroundResource(R.drawable.gold);
		al3.setBackgroundResource(R.drawable.gold);
		al4.setBackgroundResource(R.drawable.gold);
		al5.setBackgroundResource(R.drawable.gold);
		al6.setBackgroundResource(R.drawable.gold);
		al7.setBackgroundResource(R.drawable.gold);
		al8.setBackgroundResource(R.drawable.gold);
		kapi.setBackgroundResource(R.drawable.kapi);
		RelativeLayout.LayoutParams lbck = new RelativeLayout.LayoutParams((screenWidth/12),(screenHeight/12));
		RelativeLayout.LayoutParams lkapi = new RelativeLayout.LayoutParams((screenWidth/10),(screenHeight/10));
		RelativeLayout.LayoutParams lgold = new RelativeLayout.LayoutParams((screenWidth/27),(screenHeight/23));
		bck1.setLayoutParams(lbck);
		bck2.setLayoutParams(lbck);
		bck3.setLayoutParams(lbck);
		al1.setLayoutParams(lgold);
		al2.setLayoutParams(lgold);
		al3.setLayoutParams(lgold);
		al4.setLayoutParams(lgold);
		al5.setLayoutParams(lgold);
		al6.setLayoutParams(lgold);
		al7.setLayoutParams(lgold);
		al8.setLayoutParams(lgold);
		kapi.setLayoutParams(lkapi);
		rlt.addView(bck1);
		rlt.addView(bck2);
		rlt.addView(bck3);
		rlt.addView(al1);
		rlt.addView(al2);
		rlt.addView(al3);
		rlt.addView(al4);
		rlt.addView(al5);
		rlt.addView(al6);
		rlt.addView(al7);
		rlt.addView(al8);
		rlt.addView(kapi);
		bck1.setX((float)(screenWidth/2.57));
	    bck1.setY((float)(screenHeight/1.45));
	    bck2.setX((float)(screenWidth/1.58));
	    bck2.setY((float)(screenHeight/1.16));
	    bck3.setX((float)(screenWidth/1.45));
	    bck3.setY((float)(screenHeight/4.13));
		al1.setX((float)(screenWidth/1.90));
	    al1.setY((float)(screenHeight/1.17));
	    al2.setX((float)(screenWidth/1.119));
	    al2.setY((float)(screenHeight/1.29));
	    al3.setX((float)(screenWidth/1.61));
	    al3.setY((float)(screenHeight/3.04));
	    al4.setX((float)(screenWidth/2.11));
	    al4.setY((float)(screenHeight/7.06));
	    al5.setX((float)(screenWidth/3.07));
	    al5.setY((float)(screenHeight/9.05));
	    al6.setX((float)(screenWidth/4.10));
	    al6.setY((float)(screenHeight/3.56));
	    al7.setX((float)(screenWidth/4.10));
	    al7.setY((float)(screenHeight/2.15));
	    al8.setX((float)(screenWidth/3.00));
	    al8.setY((float)(screenHeight/2.15));
	    kapi.setX((float)(screenWidth/7.10));
	    kapi.setY((float)(screenHeight/2.25));
		devil = (ImageView)findViewById(R.id.devil);
		devil.setOnTouchListener(this);
		 RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((screenWidth/12),(screenHeight/12));
		    devil.setLayoutParams(layoutParams);
		    devil.setX((float)(screenWidth/183));
		    devil.setY((float)(screenHeight/1.23));
		    devil.bringToFront();
    	    hbck1();
    	    hbck2();
    	    hbck3();
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		 switch (event.getAction() & MotionEvent.ACTION_MASK) {
	        case MotionEvent.ACTION_DOWN:
	        {
	        	move=true;
				break;
			}
	        case MotionEvent.ACTION_UP:
	        {
	        	move=false;
	        	break;
	        }
	        case MotionEvent.ACTION_MOVE:
	        {
	        	String i1x=new DecimalFormat("##.#").format((screenWidth/bck1.getX()));
	        	String i1y=new DecimalFormat("##.#").format((screenWidth/bck1.getY()));
	        	String i2x=new DecimalFormat("##.#").format((screenWidth/bck2.getX()));
	        	String i2y=new DecimalFormat("##.#").format((screenWidth/bck2.getY()));
	        	String i3x=new DecimalFormat("##.#").format((screenWidth/bck3.getX()));
	        	String i3y=new DecimalFormat("##.#").format((screenWidth/bck3.getY()));
	        	String devilx=new DecimalFormat("##.#").format((screenWidth/devil.getX()));
	        	String devily=new DecimalFormat("##.#").format((screenWidth/devil.getY()));
	        	if(move)
	        	{
	        		nx=event.getRawX()-devil.getWidth()/2+10;
		        	ny=event.getRawY()-devil.getHeight()/2-40;
		        	if(screenWidth/nx<653.221 &&screenWidth/nx>1.13 && screenHeight/ny>1.17 &&screenHeight/ny<1.24
		        	||screenWidth/nx<653.221&&screenWidth/nx>1.13&& screenHeight/ny>1.17 &&screenHeight/ny<1.47
		        	||screenWidth/nx<653.221 &&screenWidth/nx>1.13&& screenHeight/ny>1.17 &&screenHeight/ny<1.24
		        	||screenWidth/nx<1.62 &&screenWidth/nx>1.13&& screenHeight/ny>1.17 &&screenHeight/ny<1.47
		        	||screenWidth/nx<1.17&&screenWidth/nx>1.13&& screenHeight/ny>1.17 &&screenHeight/ny<4.31
		        	||screenWidth/nx<1.46 &&screenWidth/nx>1.13&& screenHeight/ny>2.47 &&screenHeight/ny<4.31
		        	||screenWidth/nx<2.24 &&screenWidth/nx>1.46&& screenHeight/ny>2.92 &&screenHeight/ny<3.34
		        	||screenWidth/nx<2.24 &&screenWidth/nx>2.10&& screenHeight/ny>1.29 &&screenHeight/ny<13.7
		        	||screenWidth/nx<4.78 &&screenWidth/nx>2.10&& screenHeight/ny>8.43 &&screenHeight/ny<13.7
		        	||screenWidth/nx<4.78 &&screenWidth/nx>4.21&& screenHeight/ny>2.13 &&screenHeight/ny<13.7
		        	||screenWidth/nx<7.91 &&screenWidth/nx>3.11&& screenHeight/ny>2.13 &&screenHeight/ny<2.35
		        	)
		        	{
		        		devil.setX(nx);
		    		    devil.setY(ny);
		    		    if(screenWidth/nx<2.16 &&screenWidth/nx>1.84)
		    		    {
		    		    al1.setVisibility(View.GONE);
		    		    a1=true;
		    		    if(ses1==true)
		    		    { goldses();
		    		     ses1=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<1.20 &&screenWidth/nx>1.12 && screenHeight/ny<1.45 &&screenHeight/ny>1.22)
		    		    {
		    		    al2.setVisibility(View.GONE);
		    		    a2=true;
		    		    if(ses2==true)
		    		    { goldses();
		    		    ses2=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<1.76 &&screenWidth/nx>1.56 && screenHeight/ny>2.81 &&screenHeight/ny<3.45)
		    		    {
		    		    al3.setVisibility(View.GONE);
		    		    a3=true;
		    		    if(ses3==true)
		    		    { goldses();
		    		    ses3=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<2.24 &&screenWidth/nx>2.10 && screenHeight/ny>5.45 &&screenHeight/ny<11.8)
		    		    {
		    		    al4.setVisibility(View.GONE);
		    		    a4=true;
		    		    if(ses4==true)
		    		    { goldses();
		    		    ses4=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<3.83 &&screenWidth/nx>2.92 && screenHeight/ny>8.01 &&screenHeight/ny<11.8)
		    		    {
		    		    al5.setVisibility(View.GONE);
		    		    a5=true;
		    		    if(ses5==true)
		    		    { goldses();
		    		    ses5=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<4.82 &&screenWidth/nx>4.20 && screenHeight/ny>3.16&&screenHeight/ny<5.02)
		    		    {
		    		    al6.setVisibility(View.GONE);
		    		    a6=true;
		    		    if(ses6==true)
		    		    { goldses();
		    		    ses6=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<4.82 &&screenWidth/nx>4.20 && screenHeight/ny>2.18 &&screenHeight/ny<2.61)
		    		    {
		    		    al7.setVisibility(View.GONE);
		    		    a7=true;
		    		    if(ses7==true)
		    		    { goldses();
		    		    ses7=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<3.70 &&screenWidth/nx>3.18 && screenHeight/ny>2.11 &&screenHeight/ny<2.31)
		    		    {
		    		    al8.setVisibility(View.GONE);
		    		    a8=true;
		    		    if(ses8==true)
		    		    { goldses();
		    		    ses8=false;
		    		    }
		    		    }
		    		    if(a1==true&&a2==true&&a3==true&&a4==true&&a5==true&&a6==true&&a7==true&&a8==true)
		    		    {
		    		    	kapi.setBackgroundResource(R.drawable.acikkapi);
		    		    }
		    		    if(screenWidth/nx<7.49 &&screenWidth/nx>6.12 && screenHeight/ny>2.16 &&screenHeight/ny<2.28)
		    		    {  if(a1==true&&a2==true&&a3==true&&a4==true&&a5==true&&a6==true&&a7==true&&a8==true)
		    		    {
		    		    	MediaPlayer a1= MediaPlayer.create(this,R.raw.kapi);
			    		    a1.start();    
			    		    a1.setOnCompletionListener(new OnCompletionListener() {
			    			    public void onCompletion(MediaPlayer mp) {
			    			        mp.release();

			    			    };
			    			});
		    		    	Intent intent = new Intent(Level3.this,Level4.class);
		    		    	startActivity(intent);
		    		    }
		    		    }
		    		    if(devilx.toString().equals(i1x)&&devily.toString().equals(i1y))
		    		    {a1=false; a2=false; a3=false; a4=false; a5=false; a6=false; a7=false; a8=false;		
		        		ses1=true; ses2=true; ses3=true; ses4=true; ses5=true; ses6=true; ses7=true; ses8=true;	
		        		al1.setVisibility(View.VISIBLE);
		        		al2.setVisibility(View.VISIBLE);
		        		al3.setVisibility(View.VISIBLE);
		        		al4.setVisibility(View.VISIBLE);
		        		al5.setVisibility(View.VISIBLE);
		        		al6.setVisibility(View.VISIBLE);
		        		al7.setVisibility(View.VISIBLE);
		        		al8.setVisibility(View.VISIBLE);
		        		devil.setX((float)(screenWidth/40));
		    		    devil.setY((float)(screenHeight/1.20));
		    		    move=false;}
		    		    if(devilx.toString().equals(i2x)&&devily.toString().equals(i2y))
		    		    {	
		        		ses1=true; ses2=true; ses3=true; ses4=true; ses5=true; ses6=true; ses7=true; ses8=true;	
		        		al1.setVisibility(View.VISIBLE);
		        		al2.setVisibility(View.VISIBLE);
		        		al3.setVisibility(View.VISIBLE);
		        		al4.setVisibility(View.VISIBLE);
		        		al5.setVisibility(View.VISIBLE);
		        		al6.setVisibility(View.VISIBLE);
		        		al7.setVisibility(View.VISIBLE);
		        		al8.setVisibility(View.VISIBLE);
		        		devil.setX((float)(screenWidth/40));
		    		    devil.setY((float)(screenHeight/1.20));
		    		    move=false;}
		    		    if(devilx.toString().equals(i3x)&&devily.toString().equals(i3y))
		    		    {a1=false; a2=false; a3=false; a4=false; a5=false; a6=false; a7=false; a8=false;		
		        		ses1=true; ses2=true; ses3=true; ses4=true; ses5=true; ses6=true; ses7=true; ses8=true;	
		        		al1.setVisibility(View.VISIBLE);
		        		al2.setVisibility(View.VISIBLE);
		        		al3.setVisibility(View.VISIBLE);
		        		al4.setVisibility(View.VISIBLE);
		        		al5.setVisibility(View.VISIBLE);
		        		al6.setVisibility(View.VISIBLE);
		        		al7.setVisibility(View.VISIBLE);
		        		al8.setVisibility(View.VISIBLE);
		        		devil.setX((float)(screenWidth/40));
		    		    devil.setY((float)(screenHeight/1.20));
		    		    move=false;}
		        	}
		        	else{
		        		a1=false; a2=false; a3=false; a4=false; a5=false; a6=false; a7=false; a8=false;		
		        		ses1=true; ses2=true; ses3=true; ses4=true; ses5=true; ses6=true; ses7=true; ses8=true;	
		        		al1.setVisibility(View.VISIBLE);
		        		al2.setVisibility(View.VISIBLE);
		        		al3.setVisibility(View.VISIBLE);
		        		al4.setVisibility(View.VISIBLE);
		        		al5.setVisibility(View.VISIBLE);
		        		al6.setVisibility(View.VISIBLE);
		        		al7.setVisibility(View.VISIBLE);
		        		al8.setVisibility(View.VISIBLE);
		        		devil.setX((float)(screenWidth/40));
		    		    devil.setY((float)(screenHeight/1.20));
		    		    move=false;
		    		   
		        	}
	        	}
	        	break;
	        }
		}
		return true;
	}
public void hbck1()
{
	bck1.setBackgroundResource(R.drawable.abck);
	TranslateAnimation  anim = new TranslateAnimation(0,0, 0,screenHeight/7);
    anim.setDuration(1000);
    anim.setFillAfter( true );
    bck1.startAnimation(anim);
    anim.setAnimationListener(new AnimationListener() {
    	   @Override
    	   public void onAnimationStart(Animation arg0) {

    	   }
    		   
	    	   @Override
	    	   public void onAnimationEnd(Animation arg0) {
	    		   bck1.setY((float)(screenHeight/1.20));
	    		   lbck1();
   }
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
});
 
}
public void lbck1()
{
	bck1.setBackgroundResource(R.drawable.lbck);
	TranslateAnimation  anim = new TranslateAnimation(0,-screenWidth/6, 0,0);
    anim.setDuration(1000);
    anim.setFillAfter( true );
    bck1.startAnimation(anim);
    anim.setAnimationListener(new AnimationListener() {
    	   @Override
    	   public void onAnimationStart(Animation arg0) {

    	   }
    		   
	    	   @Override
	    	   public void onAnimationEnd(Animation arg0) {
	    		   bck1.setX((float)(screenWidth/4.51));
	    		   ybck1();
   }
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
});
 
}
public void ybck1()
{
	bck1.setBackgroundResource(R.drawable.ybck);
	TranslateAnimation  anim = new TranslateAnimation(0,0, 0,-screenHeight/6);
    anim.setDuration(1000);
    anim.setFillAfter( true );
    bck1.startAnimation(anim);
    anim.setAnimationListener(new AnimationListener() {
    	   @Override
    	   public void onAnimationStart(Animation arg0) {

    	   }
    		   
	    	   @Override
	    	   public void onAnimationEnd(Animation arg0) {
	    		   bck1.setY((float)(screenHeight/1.44));
	    		   rbck1();
   }
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
});
 
}
public void rbck1()
{
	bck1.setBackgroundResource(R.drawable.rbck);
	TranslateAnimation  anim = new TranslateAnimation(0,screenWidth/6, 0,0);
    anim.setDuration(1000);
    anim.setFillAfter( true );
    bck1.startAnimation(anim);
    anim.setAnimationListener(new AnimationListener() {
    	   @Override
    	   public void onAnimationStart(Animation arg0) {

    	   }
    		   
	    	   @Override
	    	   public void onAnimationEnd(Animation arg0) {
	    		   bck1.setX((float)(screenWidth/2.70)); 
	    		   hbck1();
   }
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
});
 
}
public void hbck2()
{
	bck2.setBackgroundResource(R.drawable.ybck);
	TranslateAnimation  anim = new TranslateAnimation(0,0, 0,-screenHeight/6);
    anim.setDuration(1000);
    anim.setFillAfter( true );
    bck2.startAnimation(anim);
    anim.setAnimationListener(new AnimationListener() {
    	   @Override
    	   public void onAnimationStart(Animation arg0) {

    	   }
    		   
	    	   @Override
	    	   public void onAnimationEnd(Animation arg0) {
	    		   bck2.setY((float)(screenHeight/1.42));
	    		   rbck2();
   }
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
});
 
}
public void rbck2()
{
	bck2.setBackgroundResource(R.drawable.rbck);
	TranslateAnimation  anim = new TranslateAnimation(0,screenWidth/6, 0,0);
    anim.setDuration(1000);
    anim.setFillAfter( true );
    bck2.startAnimation(anim);
    anim.setAnimationListener(new AnimationListener() {
    	   @Override
    	   public void onAnimationStart(Animation arg0) {

    	   }
    		   
	    	   @Override
	    	   public void onAnimationEnd(Animation arg0) {
	    		   bck2.setX((float)(screenWidth/1.25)); 
	    		   abck2();
   }
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
});
 
}
public void abck2()
{
	bck2.setBackgroundResource(R.drawable.abck);
	TranslateAnimation  anim = new TranslateAnimation(0,0, 0,screenHeight/6);
    anim.setDuration(1000);
    anim.setFillAfter( true );
    bck2.startAnimation(anim);
    anim.setAnimationListener(new AnimationListener() {
    	   @Override
    	   public void onAnimationStart(Animation arg0) {

    	   }
    		   
	    	   @Override
	    	   public void onAnimationEnd(Animation arg0) {
	    		   bck2.setY((float)(screenHeight/1.16));
	    		  lbck2();
   }
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
});
 
}
public void lbck2()
{
	bck2.setBackgroundResource(R.drawable.lbck);
	TranslateAnimation  anim = new TranslateAnimation(0,-screenWidth/6, 0,0);
    anim.setDuration(1000);
    anim.setFillAfter( true );
    bck2.startAnimation(anim);
    anim.setAnimationListener(new AnimationListener() {
    	   @Override
    	   public void onAnimationStart(Animation arg0) {

    	   }
    		   
	    	   @Override
	    	   public void onAnimationEnd(Animation arg0) {
	    		   bck2.setX((float)(screenWidth/1.61)); 
	    		   hbck2();
   }
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
});
 
}
public void hbck3()
{
	bck3.setBackgroundResource(R.drawable.rbck);
	TranslateAnimation  anim = new TranslateAnimation(0,screenWidth/6, 0,0);
    anim.setDuration(1000);
    anim.setFillAfter( true );
    bck3.startAnimation(anim);
    anim.setAnimationListener(new AnimationListener() {
    	   @Override
    	   public void onAnimationStart(Animation arg0) {

    	   }
    		   
	    	   @Override
	    	   public void onAnimationEnd(Animation arg0) {
	    		   bck3.setX((float)(screenWidth/1.16)); 
	    		   abck3();
   }
	@Override
	public void onAnimationRepeat(Animation animation) {
		
	}
});
 
}
public void abck3()
{
	bck3.setBackgroundResource(R.drawable.abck);
	TranslateAnimation  anim = new TranslateAnimation(0,0, 0,screenHeight/6);
    anim.setDuration(1000);
    anim.setFillAfter( true );
    bck3.startAnimation(anim);
    anim.setAnimationListener(new AnimationListener() {
    	   @Override
    	   public void onAnimationStart(Animation arg0) {

    	   }
    		   
	    	   @Override
	    	   public void onAnimationEnd(Animation arg0) {
	    		   bck3.setY((float)(screenHeight/2.35));
	    		   lbck3();
   }
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
});
 
}
public void lbck3()
{
	bck3.setBackgroundResource(R.drawable.lbck);
	TranslateAnimation  anim = new TranslateAnimation(0,-screenWidth/6, 0,0);
    anim.setDuration(1000);
    anim.setFillAfter( true );
    bck3.startAnimation(anim);
    anim.setAnimationListener(new AnimationListener() {
    	   @Override
    	   public void onAnimationStart(Animation arg0) {

    	   }
    		   
	    	   @Override
	    	   public void onAnimationEnd(Animation arg0) {
	    		   bck3.setX((float)(screenWidth/1.44)); 
	    		   ybck3();
   }
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
});
 
}
public void ybck3()
{
	bck3.setBackgroundResource(R.drawable.ybck);
	TranslateAnimation  anim = new TranslateAnimation(0,0, 0,-screenHeight/6);
    anim.setDuration(1000);
    anim.setFillAfter( true );
    bck3.startAnimation(anim);
    anim.setAnimationListener(new AnimationListener() {
    	   @Override
    	   public void onAnimationStart(Animation arg0) {

    	   }
    		   
	    	   @Override
	    	   public void onAnimationEnd(Animation arg0) {
	    		   bck3.setY((float)(screenHeight/4.04));
	    		  hbck3();
   }
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
});
 
}
public void goldses()
{
	MediaPlayer a1= MediaPlayer.create(this,R.raw.gold);
    a1.start();    
    a1.setOnCompletionListener(new OnCompletionListener() {
	    public void onCompletion(MediaPlayer mp) {
	        mp.release();

	    };
	});
}
public void displayInterstitial() {
	// If Ads are loaded, show Interstitial else show nothing.
	if (interstitial.isLoaded()) {
		interstitial.show();
	}
}
}
