package com.gojolo.thehardestgame;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class Level8 extends Activity implements View.OnTouchListener{

	ImageView devil;
	ImageView al1,al2,al3,al4,al5,al6,al7,al8,kapi;
static	ImageView bck1,bck2,bck3;
	boolean a1=false,a2=false,a3=false,a4=false,a5=false,a6=false,a7=false,a8=false;
	boolean ses1=true,ses2=true,ses3=true,ses4=true,ses5=true,ses6=true,ses7=true,ses8=true;
	boolean move;
	float nx,ny;
	int screenHeight,screenWidth;
	private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_level8);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		AdRequest adRequest = new AdRequest.Builder().build(); 
	    interstitial = new InterstitialAd(Level8.this);
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/4654726241");
		interstitial.loadAd(adRequest);
				interstitial.setAdListener(new AdListener() {
					public void onAdLoaded() {
						displayInterstitial();
					}
				});
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		RelativeLayout rlt = (RelativeLayout)findViewById(R.id.rlt);
		al1 = new ImageView(this);
		al2 = new ImageView(this);
		al3 = new ImageView(this);
		al4 = new ImageView(this);
		al5 = new ImageView(this);
		al6 = new ImageView(this);
		al7 = new ImageView(this);
		al8 = new ImageView(this);
		kapi = new ImageView(this);
		al1.setBackgroundResource(R.drawable.gold);
		al2.setBackgroundResource(R.drawable.gold);
		al3.setBackgroundResource(R.drawable.gold);
		al4.setBackgroundResource(R.drawable.gold);
		al5.setBackgroundResource(R.drawable.gold);
		al6.setBackgroundResource(R.drawable.gold);
		al7.setBackgroundResource(R.drawable.gold);
		al8.setBackgroundResource(R.drawable.gold);
		kapi.setBackgroundResource(R.drawable.kapi);
		RelativeLayout.LayoutParams lkapi = new RelativeLayout.LayoutParams((screenWidth/12),(screenHeight/12));
		RelativeLayout.LayoutParams lgold = new RelativeLayout.LayoutParams((screenWidth/27),(screenHeight/23));
		al1.setLayoutParams(lgold);
		al2.setLayoutParams(lgold);
		al3.setLayoutParams(lgold);
		al4.setLayoutParams(lgold);
		al5.setLayoutParams(lgold);
		al6.setLayoutParams(lgold);
		al7.setLayoutParams(lgold);
		al8.setLayoutParams(lgold);
		kapi.setLayoutParams(lkapi);
		rlt.addView(al1);
		rlt.addView(al2);
		rlt.addView(al3);
		rlt.addView(al4);
		rlt.addView(al5);
		rlt.addView(al6);
		rlt.addView(al7);
		rlt.addView(al8);
		rlt.addView(kapi);
		al1.setX((float)(screenWidth/2.27));
	    al1.setY((float)(screenHeight/4.34));
	    al2.setX((float)(screenWidth/2.27));
	    al2.setY((float)(screenHeight/2.00));
	    al3.setX((float)(screenWidth/6.00));
	    al3.setY((float)(screenHeight/2.71));
	    al4.setX((float)(screenWidth/6.00));
	    al4.setY((float)(screenHeight/17.12));
	    al5.setX((float)(screenWidth/1.63));
	    al5.setY((float)(screenHeight/17.00));
	    al6.setX((float)(screenWidth/6.00));
	    al6.setY((float)(screenHeight/1.34));
	    al7.setX((float)(screenWidth/2.64));
	    al7.setY((float)(screenHeight/1.34));
	    al8.setX((float)(screenWidth/1.65));
	    al8.setY((float)(screenHeight/1.33));
	    kapi.setX((float)(screenWidth/1.41));
	    kapi.setY((float)(screenHeight/1.61));
		devil = (ImageView)findViewById(R.id.devil);
		devil.setOnTouchListener(this);
		 RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((screenWidth/12),(screenHeight/12));
		    devil.setLayoutParams(layoutParams);
		    devil.setX((float)(screenWidth/1.20));
		    devil.setY((float)(screenHeight/1.62));
		    devil.bringToFront();
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		 switch (event.getAction() & MotionEvent.ACTION_MASK) {
	        case MotionEvent.ACTION_DOWN:
	        {
	        	move=true;
				break;
			}
	        case MotionEvent.ACTION_UP:
	        {
	        	move=false;
	        	break;
	        }
	        case MotionEvent.ACTION_MOVE:
	        {
	        	if(move)
	        	{
		        	nx=event.getRawX()-devil.getWidth()/2+10;
		        	ny=event.getRawY()-devil.getHeight()/2-40;
		        	devil.setX(nx);
	    		    devil.setY(ny);
		        	if(screenWidth/nx<1.22 &&screenWidth/nx>1.17 && screenHeight/ny>1.56 &&screenHeight/ny<4.77
		        	||screenWidth/nx<7.44 &&screenWidth/nx>1.17&& screenHeight/ny>3.86 &&screenHeight/ny<4.77
		        	||screenWidth/nx<1.43 &&screenWidth/nx>1.38&& screenHeight/ny>1.56 &&screenHeight/ny<4.77
		        	||screenWidth/nx<2.45 &&screenWidth/nx>2.27&& screenHeight/ny>1.95 &&screenHeight/ny<4.69
		        	||screenWidth/nx<7.44 &&screenWidth/nx>2.27&& screenHeight/ny>1.95 &&screenHeight/ny<2.14
		        	||screenWidth/nx<7.44 &&screenWidth/nx>6.07&& screenHeight/ny>1.30 &&screenHeight/ny<24.26
		        	||screenWidth/nx<7.44 &&screenWidth/nx>1.60&& screenHeight/ny>1.29 &&screenHeight/ny<1.39
		        	||screenWidth/nx<7.44 &&screenWidth/nx>1.60&& screenHeight/ny>11.84 &&screenHeight/ny<26.60)
		        	{
		        		devil.setX(nx);
		    		    devil.setY(ny);
		    		    if(screenWidth/nx<1.83 &&screenWidth/nx>1.71 && screenHeight/ny>1.31 &&screenHeight/ny<1.38)
		    		    {
		    		    al8.setVisibility(View.GONE);
		    		    a8=true;
		    		    if(ses8==true)
		    		    { goldses();
		    		     ses8=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<2.41 &&screenWidth/nx>2.19 && screenHeight/ny<4.76 &&screenHeight/ny>4.32)
		    		    {
		    		    al1.setVisibility(View.GONE);
		    		    a1=true;
		    		    if(ses1==true)
		    		    { goldses();
		    		    ses1=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<2.41 &&screenWidth/nx>2.19 && screenHeight/ny>2.13 &&screenHeight/ny<2.38)
		    		    {
		    		    al2.setVisibility(View.GONE);
		    		    a2=true;
		    		    if(ses2==true)
		    		    { goldses();
		    		    ses2=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<7.36 &&screenWidth/nx>6.43 && screenHeight/ny>2.48 &&screenHeight/ny<2.87)
		    		    {
		    		    al3.setVisibility(View.GONE);
		    		    a3=true;
		    		    if(ses3==true)
		    		    { goldses();
		    		    ses3=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<7.36 &&screenWidth/nx>6.43  && screenHeight/ny>10.55 &&screenHeight/ny<22.68)
		    		    {
		    		    al4.setVisibility(View.GONE);
		    		    a4=true;
		    		    if(ses4==true)
		    		    { goldses();
		    		    ses4=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<1.81 &&screenWidth/nx>1.68 && screenHeight/ny>10.55 &&screenHeight/ny<22.68)
		    		    {
		    		    al5.setVisibility(View.GONE);
		    		    a5=true;
		    		    if(ses5==true)
		    		    { goldses();
		    		    ses5=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<7.36 &&screenWidth/nx>6.43 && screenHeight/ny>1.40 &&screenHeight/ny<1.51)
		    		    {
		    		    al6.setVisibility(View.GONE);
		    		    a6=true;
		    		    if(ses6==true)
		    		    { goldses();
		    		    ses6=false;
		    		    }
		    		    }
		    		    if(screenWidth/nx<3.22 &&screenWidth/nx>3.07 && screenHeight/ny>1.34 &&screenHeight/ny<1.44)
		    		    {
		    		    al7.setVisibility(View.GONE);
		    		    a7=true;
		    		    if(ses7==true)
		    		    { goldses();
		    		    ses7=false;
		    		    }
		    		    }
		    		    if(a1==true&&a2==true&&a3==true&&a4==true&&a5==true&&a6==true&&a7==true&&a8==true)
		    		    {
		    		    	kapi.setBackgroundResource(R.drawable.acikkapi);
		    		    }
		    		    if(screenWidth/nx<1.42 &&screenWidth/nx>1.38 && screenHeight/ny>1.55 &&screenHeight/ny<1.62)
		    		    {  if(a1==true&&a2==true&&a3==true&&a4==true&&a5==true&&a6==true&&a7==true&&a8==true)
		    		    {
		    		    	MediaPlayer a1= MediaPlayer.create(this,R.raw.kapi);
			    		    a1.start();    
			    		    a1.setOnCompletionListener(new OnCompletionListener() {
			    			    public void onCompletion(MediaPlayer mp) {
			    			        mp.release();

			    			    };
			    			});
		    		    	Intent intent = new Intent(Level8.this,Level9.class);
		    		    	startActivity(intent);
		    		    }
		    		    }
		    		    
		        	}
		        	else{
		        		a1=false; a2=false; a3=false; a4=false; a5=false; a6=false; a7=false; a8=false;		
		        		ses1=true; ses2=true; ses3=true; ses4=true; ses5=true; ses6=true; ses7=true; ses8=true;	
		        		al1.setVisibility(View.VISIBLE);
		        		al2.setVisibility(View.VISIBLE);
		        		al3.setVisibility(View.VISIBLE);
		        		al4.setVisibility(View.VISIBLE);
		        		al5.setVisibility(View.VISIBLE);
		        		al6.setVisibility(View.VISIBLE);
		        		al7.setVisibility(View.VISIBLE);
		        		al8.setVisibility(View.VISIBLE);
		    		    devil.setX((float)(screenWidth/1.20));
		    		    devil.setY((float)(screenHeight/1.62));
		    		    move=false;
		    		    
		        	}
		        	}
	        	break;
	        }
		}
		return true;
	}
	public void goldses()
	{
		MediaPlayer a1= MediaPlayer.create(this,R.raw.gold);
	    a1.start();    
	    a1.setOnCompletionListener(new OnCompletionListener() {
		    public void onCompletion(MediaPlayer mp) {
		        mp.release();

		    };
		});
	}
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
}
