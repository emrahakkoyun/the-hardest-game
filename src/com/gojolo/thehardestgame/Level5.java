package com.gojolo.thehardestgame;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class Level5 extends Activity implements View.OnTouchListener{
		ImageView devil;
		ImageView al1,al2,al3,al4,al5,al6,al7,al8,kapi;
	static	ImageView bck1,bck2,bck3;
		boolean a1=false,a2=false,a3=false,a4=false,a5=false,a6=false,a7=false,a8=false;
		boolean ses1=true,ses2=true,ses3=true,ses4=true,ses5=true,ses6=true,ses7=true,ses8=true;
		boolean move;
		float nx,ny;
		int screenHeight,screenWidth;
		private InterstitialAd interstitial;
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_level5);
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			AdRequest adRequest = new AdRequest.Builder().build(); 
		    interstitial = new InterstitialAd(Level5.this);
			interstitial.setAdUnitId("ca-app-pub-1312048647642571/3317593841");
			interstitial.loadAd(adRequest);
					interstitial.setAdListener(new AdListener() {
						public void onAdLoaded() {
							displayInterstitial();
						}
					});
			DisplayMetrics displaymetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
			screenHeight = displaymetrics.heightPixels;
			screenWidth = displaymetrics.widthPixels;
			RelativeLayout rlt = (RelativeLayout)findViewById(R.id.rlt);
			al1 = new ImageView(this);
			al2 = new ImageView(this);
			al3 = new ImageView(this);
			al4 = new ImageView(this);
			al5 = new ImageView(this);
			al6 = new ImageView(this);
			al7 = new ImageView(this);
			al8 = new ImageView(this);
			kapi = new ImageView(this);
			al1.setBackgroundResource(R.drawable.gold);
			al2.setBackgroundResource(R.drawable.gold);
			al3.setBackgroundResource(R.drawable.gold);
			al4.setBackgroundResource(R.drawable.gold);
			al5.setBackgroundResource(R.drawable.gold);
			al6.setBackgroundResource(R.drawable.gold);
			al7.setBackgroundResource(R.drawable.gold);
			al8.setBackgroundResource(R.drawable.gold);
			kapi.setBackgroundResource(R.drawable.kapi);
			RelativeLayout.LayoutParams lkapi = new RelativeLayout.LayoutParams((screenWidth/10),(screenHeight/10));
			RelativeLayout.LayoutParams lgold = new RelativeLayout.LayoutParams((screenWidth/27),(screenHeight/23));
			al1.setLayoutParams(lgold);
			al2.setLayoutParams(lgold);
			al3.setLayoutParams(lgold);
			al4.setLayoutParams(lgold);
			al5.setLayoutParams(lgold);
			al6.setLayoutParams(lgold);
			al7.setLayoutParams(lgold);
			al8.setLayoutParams(lgold);
			kapi.setLayoutParams(lkapi);
			rlt.addView(al1);
			rlt.addView(al2);
			rlt.addView(al3);
			rlt.addView(al4);
			rlt.addView(al5);
			rlt.addView(al6);
			rlt.addView(al7);
			rlt.addView(al8);
			rlt.addView(kapi);
			al1.setX((float)(screenWidth/4.51));
		    al1.setY((float)(screenHeight/2.20));
		    al2.setX((float)(screenWidth/2.55));
		    al2.setY((float)(screenHeight/3.26));
		    al3.setX((float)(screenWidth/1.95));
		    al3.setY((float)(screenHeight/7.33));
		    al4.setX((float)(screenWidth/1.42));
		    al4.setY((float)(screenHeight/3.07));
		    al5.setX((float)(screenWidth/1.24));
		    al5.setY((float)(screenHeight/1.54));
		    al6.setX((float)(screenWidth/1.16));
		    al6.setY((float)(screenHeight/7.62));
		    al7.setX((float)(screenWidth/1.84));
		    al7.setY((float)(screenHeight/3.24));
		    al8.setX((float)(screenWidth/7.25));
		    al8.setY((float)(screenHeight/1.56));
		    kapi.setX((float)(screenWidth/7.95));
		    kapi.setY((float)(screenHeight/1.29));
			devil = (ImageView)findViewById(R.id.devil);
			devil.setOnTouchListener(this);
			 RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((screenWidth/12),(screenHeight/12));
			    devil.setLayoutParams(layoutParams);
			    devil.setX((float)(screenWidth/13.63));
			    devil.setY((float)(screenHeight/8.84));
			    devil.bringToFront();
		}
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			 switch (event.getAction() & MotionEvent.ACTION_MASK) {
		        case MotionEvent.ACTION_DOWN:
		        {
		        	move=true;
					break;
				}
		        case MotionEvent.ACTION_UP:
		        {
		        	move=false;
		        	break;
		        }
		        case MotionEvent.ACTION_MOVE:
		        {
		        	if(move)
		        	{
			        	nx=event.getRawX()-devil.getWidth()/2+10;
			        	ny=event.getRawY()-devil.getHeight()/2-40;
			        	devil.setX(nx);
		    		    devil.setY(ny);
			        	if(screenWidth/nx<18.34 &&screenWidth/nx>7.93 && screenHeight/ny>2.15 &&screenHeight/ny<9.52
			        	||screenWidth/nx<17.79 &&screenWidth/nx>2.55&& screenHeight/ny>2.15 &&screenHeight/ny<2.40
			        	||screenWidth/nx<2.77 &&screenWidth/nx>2.55&& screenHeight/ny>2.15 &&screenHeight/ny<9.31
			        	||screenWidth/nx<2.77 &&screenWidth/nx>1.43&& screenHeight/ny>6.57 &&screenHeight/ny<9.80
			        	||screenWidth/nx<1.49 &&screenWidth/nx>1.43&& screenHeight/ny>1.50 &&screenHeight/ny<9.50
			        	||screenWidth/nx<8.89 &&screenWidth/nx>1.16&& screenHeight/ny>1.51 &&screenHeight/ny<1.63
			        	||screenWidth/nx<1.20 &&screenWidth/nx>1.16&& screenHeight/ny>1.51 &&screenHeight/ny<9.31
			        	||screenWidth/nx<1.94 &&screenWidth/nx>1.84&& screenHeight/ny>1.51 &&screenHeight/ny<3.41
			        	||screenWidth/nx<8.96 &&screenWidth/nx>6.27&& screenHeight/ny>1.25 &&screenHeight/ny<1.62)
			        	{
			        		devil.setX(nx);
			    		    devil.setY(ny);
			    		    if(screenWidth/nx<6.33 &&screenWidth/nx>4.22 && screenHeight/ny>2.18 &&screenHeight/ny<2.35)
			    		    {
			    		    al1.setVisibility(View.GONE);
			    		    a1=true;
			    		    if(ses1==true)
			    		    { goldses();
			    		     ses1=false;
			    		    }
			    		    }
			    		    if(screenWidth/nx<2.68 &&screenWidth/nx>2.58 && screenHeight/ny<3.10 &&screenHeight/ny>2.85)
			    		    {
			    		    al2.setVisibility(View.GONE);
			    		    a2=true;
			    		    if(ses2==true)
			    		    { goldses();
			    		    ses2=false;
			    		    }
			    		    }
			    		    if(screenWidth/nx<2.22 &&screenWidth/nx>1.94 && screenHeight/ny>7.18 &&screenHeight/ny<9.05)
			    		    {
			    		    al3.setVisibility(View.GONE);
			    		    a3=true;
			    		    if(ses3==true)
			    		    { goldses();
			    		    ses3=false;
			    		    }
			    		    }
			    		    if(screenWidth/nx<1.48 &&screenWidth/nx>1.44 && screenHeight/ny>3.24 &&screenHeight/ny<4.00)
			    		    {
			    		    al4.setVisibility(View.GONE);
			    		    a4=true;
			    		    if(ses4==true)
			    		    { goldses();
			    		    ses4=false;
			    		    }
			    		    }
			    		    if(screenWidth/nx<1.35 &&screenWidth/nx>1.30 && screenHeight/ny>1.54 &&screenHeight/ny<1.61)
			    		    {
			    		    al5.setVisibility(View.GONE);
			    		    a5=true;
			    		    if(ses5==true)
			    		    { goldses();
			    		    ses5=false;
			    		    }
			    		    }
			    		    if(screenWidth/nx<1.19 &&screenWidth/nx>1.17 && screenHeight/ny>6.01 &&screenHeight/ny<8.10)
			    		    {
			    		    al6.setVisibility(View.GONE);
			    		    a6=true;
			    		    if(ses6==true)
			    		    { goldses();
			    		    ses6=false;
			    		    }
			    		    }
			    		    if(screenWidth/nx<1.92 &&screenWidth/nx>1.86 && screenHeight/ny>2.87 &&screenHeight/ny<3.30)
			    		    {
			    		    al7.setVisibility(View.GONE);
			    		    a7=true;
			    		    if(ses7==true)
			    		    { goldses();
			    		    ses7=false;
			    		    }
			    		    }
			    		    if(screenWidth/nx<8.72 &&screenWidth/nx>6.64 && screenHeight/ny>1.50 &&screenHeight/ny<1.59)
			    		    {
			    		    al8.setVisibility(View.GONE);
			    		    a8=true;
			    		    if(ses8==true)
			    		    { goldses();
			    		    ses8=false;
			    		    }
			    		    }
			    		    if(a1==true&&a2==true&&a3==true&&a4==true&&a5==true&&a6==true&&a7==true&&a8==true)
			    		    {
			    		    	kapi.setBackgroundResource(R.drawable.acikkapi);
			    		    }
			    		    if(screenWidth/nx<8.24 &&screenWidth/nx>6.81 && screenHeight/ny>1.25 &&screenHeight/ny<1.29)
			    		    {  if(a1==true&&a2==true&&a3==true&&a4==true&&a5==true&&a6==true&&a7==true&&a8==true)
			    		    {
			    		    	MediaPlayer a1= MediaPlayer.create(this,R.raw.kapi);
				    		    a1.start();    
				    		    a1.setOnCompletionListener(new OnCompletionListener() {
				    			    public void onCompletion(MediaPlayer mp) {
				    			        mp.release();

				    			    };
				    			});
			    		    	Intent intent = new Intent(Level5.this,Level6.class);
			    		    	startActivity(intent);
			    		    }
			    		    }
			        	}
			        	else{
			        		a1=false; a2=false; a3=false; a4=false; a5=false; a6=false; a7=false; a8=false;		
			        		ses1=true; ses2=true; ses3=true; ses4=true; ses5=true; ses6=true; ses7=true; ses8=true;	
			        		al1.setVisibility(View.VISIBLE);
			        		al2.setVisibility(View.VISIBLE);
			        		al3.setVisibility(View.VISIBLE);
			        		al4.setVisibility(View.VISIBLE);
			        		al5.setVisibility(View.VISIBLE);
			        		al6.setVisibility(View.VISIBLE);
			        		al7.setVisibility(View.VISIBLE);
			        		al8.setVisibility(View.VISIBLE);
			        		devil.setX((float)(screenWidth/13.63));
			    		    devil.setY((float)(screenHeight/8.84));
			    		    move=false;
			    		    
			        	}
			        	}
		        	break;
		        }
			}
			return true;
		}
		public void goldses()
		{
			MediaPlayer a1= MediaPlayer.create(this,R.raw.gold);
		    a1.start();    
		    a1.setOnCompletionListener(new OnCompletionListener() {
			    public void onCompletion(MediaPlayer mp) {
			        mp.release();

			    };
			});
		}
		public void displayInterstitial() {
			// If Ads are loaded, show Interstitial else show nothing.
			if (interstitial.isLoaded()) {
				interstitial.show();
			}
		}
}